function checkInp(){
    let inp = document.getElementById("kol").value;
    if(inp.match(/^[0-9]+$/) === null){
        document.getElementById("price").innerHTML = "Неверные данные";
        return false;
    }else{
        return true;
    }
}
function getPrices() {
    return {
        typeAudi: [4000000, 5000000, 6500000],
        prodOptions: {
            option1 : 0,
            option2: 1000000,
            option3: 1500000,
        },
        prodProperties: {
            prop1: 125000,
            prop2: 350000,
        },
    };
}
function price(){
    let sel = document.getElementById("typeAudi");
    let kol = document.getElementById("kol").value;
    let radioDiv = document.getElementById("radios");
    let boxes = document.getElementById("checkboxes");
    let price = document.getElementById("price");
    price.innerHTML = "0";
    if (sel.value=="1") {
            radioDiv.style.display = "none";
            boxes.style.display = "none";
            if(checkInp()){
                let all = parseInt(kol) * getPrices().typeAudi[0];
                price.innerHTML = all + " рублей";
            }
        }
       if(sel.value=="2")
       {
            radioDiv.style.display = "block";
            boxes.style.display = "none";
            let a;
            let rad = document.getElementsByName("prodOptions");
            for (var i = 0; i < rad.length; i++) {
                if (rad[i].checked){
                    a = rad[i].value;
                }
            }
            let priOfOp = getPrices().prodOptions[a];
            if(checkInp()){
                let all = parseInt(kol)*(getPrices().typeAudi[1] + priOfOp);
                price.innerHTML = all + "рублей";
            }
        }
        if(sel.value=="3")
        {
            radioDiv.style.display = "none";
            boxes.style.display = "block";
            let sum = 0;
            let p = document.getElementsByName("prop");
            for (let i = 0; i < p.length ; i++) {
                if(p[i].checked){
                    sum += getPrices().prodProperties[p[i].value];
                }
            }

            if(checkInp()){
                let all = parseInt(kol) * (getPrices().typeAudi[2] + sum);
                price.innerHTML = all + " рублей";
            }
        }
    }

window.addEventListener("DOMContentLoaded", function (event) {
    let radioDiv = document.getElementById("radios");
    radioDiv.style.display = "none";
    let boxes = document.getElementById("checkboxes");
    boxes.style.display = "none";

    let kol = document.getElementById("kol");
    kol.addEventListener("change", function (event) {
        console.log("Kol was changed");
        price();
    });

    let select = document.getElementById("typeAudi");
    select.addEventListener("change", function (event) {
        console.log("Model was changed");
        price();
    });

    let radios = document.getElementsByName("prodOptions");
    radios.forEach(function (radio) {
        radio.addEventListener("change", function (event) {
            console.log("RAM was changed");
            price();
        });
    });
    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function (checkbox) {
        checkbox.addEventListener("change", function (event) {
            console.log("Options was changed");
            price();
        });
    });
    price();
});